﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace FASE2
{
    public class Conexion
    {
        public string conexionSQL = @"Data Source=LEVI; Initial Catalog = Fase2; Integrated Security=True;";
        SqlConnection conexion;

        public void sqlQuery(string query)
        {
            SqlCommand comando = new SqlCommand(query, conexion);
            comando.ExecuteNonQuery();
        }

        public void sqlConexionAbierta()
        {
            conexion = new SqlConnection(conexionSQL);
            conexion.Open();
        }

        public void sqlCerrarConexion()
        {
            conexion.Close();
        }
    }
}