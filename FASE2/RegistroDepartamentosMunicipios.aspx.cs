﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Xml;

namespace FASE2
{
    public partial class RegistroDepartamentosMunicipios : System.Web.UI.Page
    {

        Conexion conexion = new Conexion();



        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {



        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLDepartamentos(path);
        }

        public void cargaXMLDepartamentos(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Departamento = ((XmlElement)nodoDefinicion).GetElementsByTagName("depto");
                    foreach (XmlElement nodoDepartamento in Departamento)
                    {
                        XmlNodeList departamentoID = nodoDepartamento.GetElementsByTagName("codigo");
                        XmlNodeList nombreDep = nodoDepartamento.GetElementsByTagName("nombre");

                        string query = "INSERT INTO Departamento(idDepartamento, departamento) VALUES (" + departamentoID[0].InnerText +
                            ", '" + nombreDep[0].InnerText + "');";

                        conexion.sqlConexionAbierta();
                        conexion.sqlQuery(query);
                        conexion.sqlCerrarConexion();
                    }
                }

                MessageBox.Show("Lectura de XML éxitosa", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLMunicipios(path);
        }

        public void cargaXMLMunicipios(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Municipio = ((XmlElement)nodoDefinicion).GetElementsByTagName("ciudad");
                    foreach (XmlElement nodoMunicipio in Municipio)
                    {
                        XmlNodeList municipioID = nodoMunicipio.GetElementsByTagName("codigo");
                        XmlNodeList nombreMuni = nodoMunicipio.GetElementsByTagName("nombre");
                        XmlNodeList cod_departamento = nodoMunicipio.GetElementsByTagName("codigo_departamento");


                        string query = "INSERT INTO Municipio(idMunicipio, municipio, departamento) VALUES("+ municipioID[0].InnerText
                            + ", '"+ nombreMuni[0].InnerText + "', "+ cod_departamento[0].InnerText + ")";

                        conexion.sqlConexionAbierta();
                        conexion.sqlQuery(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cargaXMLCategorias(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Categoria = ((XmlElement)nodoDefinicion).GetElementsByTagName("categoria");
                    foreach (XmlElement nodoCategoria in Categoria)
                    {
                        XmlNodeList categoriaID = nodoCategoria.GetElementsByTagName("codigo");
                        XmlNodeList nombre = nodoCategoria.GetElementsByTagName("nombre");


                        string query = "INSERT INTO Categoria (idCategoria, categoria) VALUES ("+ categoriaID[0].InnerText
                            + ", '"+ nombre[0].InnerText + "');";

                        conexion.sqlConexionAbierta();
                        conexion.sqlQuery(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cargaXMLProductos(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Producto = ((XmlElement)nodoDefinicion).GetElementsByTagName("producto");
                    foreach (XmlElement nodoProducto in Producto)
                    {
                        XmlNodeList productoID = nodoProducto.GetElementsByTagName("codigo");
                        XmlNodeList nombre = nodoProducto.GetElementsByTagName("nombre");
                        XmlNodeList descripcion = nodoProducto.GetElementsByTagName("descripcion");
                        XmlNodeList categoria = nodoProducto.GetElementsByTagName("categoria");


                        string query = "INSERT INTO Producto (idProducto, nombre, descripcion, categoria) VALUES " +
                            "("+ productoID[0].InnerText + ", '"+ nombre[0].InnerText + "', '"+ descripcion[0].InnerText + 
                            "', "+ categoria[0].InnerText + ")";

                        conexion.sqlConexionAbierta();
                        conexion.sqlQuery(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cargaXMLListas(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Lista = ((XmlElement)nodoDefinicion).GetElementsByTagName("lista");
                    foreach (XmlElement nodoLista in Lista)
                    {
                        XmlNodeList listaID = nodoLista.GetElementsByTagName("codigo");
                        XmlNodeList nombreLista = nodoLista.GetElementsByTagName("nombre");
                        XmlNodeList fechaIni = nodoLista.GetElementsByTagName("fecha_inicio");
                        XmlNodeList fechaFin = nodoLista.GetElementsByTagName("fecha_fin");

                    string query = "INSERT INTO Lista (idLista, nombre, fechaInicio, fechaFin)" +
                        " VALUES ("+ listaID[0].InnerText +", '"+ nombreLista[0].InnerText +"', '"+ fechaIni[0].InnerText +"', '"+ fechaFin[0].InnerText +"')";
;

                    conexion.sqlConexionAbierta();
                    conexion.sqlQuery(query);
                    conexion.sqlCerrarConexion();

                    XmlNodeList Detalle = ((XmlElement)nodoLista).GetElementsByTagName("detalle");
                        foreach (XmlElement nodoDetalle in Detalle)
                        {
                            XmlNodeList Item = ((XmlElement)nodoDetalle).GetElementsByTagName("item");
                            foreach (XmlElement nodoItem in Item)
                            {
                                XmlNodeList codigoProd = nodoItem.GetElementsByTagName("codigo_producto");
                                XmlNodeList precio = nodoItem.GetElementsByTagName("valor");


                                string query2 = "INSERT INTO ListaProductos(lista, producto, precio)" +
                                " VALUES ("+ listaID[0].InnerText +", "+ codigoProd[0].InnerText +", "+ precio[0].InnerText +")";

                                conexion.sqlConexionAbierta();
                                conexion.sqlQuery(query2);
                                conexion.sqlCerrarConexion();

                            }
                        }


                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cargaXMLClientes(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Cliente = ((XmlElement)nodoDefinicion).GetElementsByTagName("cliente");
                    foreach (XmlElement nodoCliente in Cliente)
                    {
                        XmlNodeList clienteNIT = nodoCliente.GetElementsByTagName("NIT");
                        XmlNodeList nombres = nodoCliente.GetElementsByTagName("nombres");
                        XmlNodeList apellidos = nodoCliente.GetElementsByTagName("apellidos");
                        XmlNodeList nacimiento = nodoCliente.GetElementsByTagName("nacimiento");
                        XmlNodeList direccion = nodoCliente.GetElementsByTagName("direccion");
                        XmlNodeList telefono = nodoCliente.GetElementsByTagName("telefono");
                        XmlNodeList celular = nodoCliente.GetElementsByTagName("celular");
                        XmlNodeList email = nodoCliente.GetElementsByTagName("email");
                        XmlNodeList municipio = nodoCliente.GetElementsByTagName("ciudad");
                        XmlNodeList departamento = nodoCliente.GetElementsByTagName("depto");
                        XmlNodeList limiteCred = nodoCliente.GetElementsByTagName("limite_credito");
                        XmlNodeList diasCred = nodoCliente.GetElementsByTagName("días_credito");
                        XmlNodeList lista = nodoCliente.GetElementsByTagName("codigo_Lista");



                        string query = "INSERT INTO Cliente VALUES" +
                            " ('75006-9', 'Fredy', 'Espinoza', '12/65/8965', " +
                            "'mi direccion', '12', '45', 'mail', 1, 501, 501," +
                            " 45.63, 5, 7775)";


                        conexion.sqlConexionAbierta();
                        conexion.sqlQuery(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "ECarga XML", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cargaXMLPuestos(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Puesto = ((XmlElement)nodoDefinicion).GetElementsByTagName("puesto");
                    foreach (XmlElement nodoPuesto in Puesto)
                    {
                        XmlNodeList puestoID = nodoPuesto.GetElementsByTagName("codigo");
                        XmlNodeList nombre = nodoPuesto.GetElementsByTagName("nombre");


                        string query = "INSERT INTO Puesto VALUES ("+ puestoID[0].InnerText +", '"+ nombre[0].InnerText +"');";

                        conexion.sqlConexionAbierta();
                        conexion.sqlQuery(query);
                        conexion.sqlCerrarConexion();
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cargaXMLEmpleados(string path)
        {

            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Empleado = ((XmlElement)nodoDefinicion).GetElementsByTagName("empleado");
                    foreach (XmlElement nodoEmpleado in Empleado)
                    {
                        XmlNodeList empleadoNIT = nodoEmpleado.GetElementsByTagName("NIT");
                        XmlNodeList nombres = nodoEmpleado.GetElementsByTagName("nombres");
                        XmlNodeList apellidos = nodoEmpleado.GetElementsByTagName("apellidos");
                        XmlNodeList nacimiento = nodoEmpleado.GetElementsByTagName("nacimiento");
                        XmlNodeList direccion = nodoEmpleado.GetElementsByTagName("direccion");
                        XmlNodeList telefono = nodoEmpleado.GetElementsByTagName("telefono");
                        XmlNodeList celular = nodoEmpleado.GetElementsByTagName("celular");
                        XmlNodeList email = nodoEmpleado.GetElementsByTagName("email");
                        XmlNodeList puesto = nodoEmpleado.GetElementsByTagName("codigo_puesto");
                        XmlNodeList jefe = nodoEmpleado.GetElementsByTagName("codigo_jefe");
                        XmlNodeList password = nodoEmpleado.GetElementsByTagName("pass");

                        if (jefe[0].InnerText == "")
                        {
                            string query = "INSERT INTO Empleado(NITempleado, nombres, apellidos, fechaNacimiento, direccion, telefono, celular," +
                             " email, puesto, passwo) " +
                             "VALUES('" + empleadoNIT[0].InnerText + "', '" + nombres[0].InnerText + "', '" + apellidos[0].InnerText + "'," +
                             " '" + nacimiento[0].InnerText + "', '" + direccion[0].InnerText + "', '" + telefono[0].InnerText + "', " +
                             "'" + celular[0].InnerText + "', '" + email[0].InnerText + "', " + puesto[0].InnerText + ", '"+ password[0].InnerText +"')";

                            conexion.sqlConexionAbierta();
                            conexion.sqlQuery(query);
                            conexion.sqlCerrarConexion();

                        }
                        else
                        {
                            string query = "INSERT INTO Empleado(NITempleado, nombres, apellidos, fechaNacimiento, direccion, telefono, celular," +
                             " email, puesto, jefe, passwo) " +
                             "VALUES('" + empleadoNIT[0].InnerText + "', '" + nombres[0].InnerText + "', '" + apellidos[0].InnerText + "'," +
                             " '" + nacimiento[0].InnerText + "', '" + direccion[0].InnerText + "', '" + telefono[0].InnerText + "', " +
                             "'" + celular[0].InnerText + "', '" + email[0].InnerText + "', " + puesto[0].InnerText + ", '" + jefe[0].InnerText + "'," +
                             " '" + password[0].InnerText + "')";

                            conexion.sqlConexionAbierta();
                            conexion.sqlQuery(query);
                            conexion.sqlCerrarConexion();
                        }
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cargaXMLMetas(string path)
        {

            //try
            //{
                XmlDocument documento = new XmlDocument();
                documento.Load(path);
                XmlNodeList Definicion = documento.GetElementsByTagName("definicion");
                foreach (XmlElement nodoDefinicion in Definicion)
                {
                    XmlNodeList Meta = ((XmlElement)nodoDefinicion).GetElementsByTagName("meta");
                    foreach (XmlElement nodoMeta in Meta)
                    {
                        XmlNodeList empleadoID = nodoMeta.GetElementsByTagName("NIT_empleado");
                        XmlNodeList mes = nodoMeta.GetElementsByTagName("mes_meta");

                        XmlNodeList Detalle = ((XmlElement)nodoMeta).GetElementsByTagName("detalle");
                        foreach (XmlElement nodoDetalle in Detalle)
                        {

                            XmlNodeList Item = ((XmlElement)nodoDetalle).GetElementsByTagName("item");
                            foreach (XmlElement nodoItem in Item)
                            {
                                XmlNodeList categoria = nodoItem.GetElementsByTagName("codigo_categoría");
                                XmlNodeList cantidad = nodoItem.GetElementsByTagName("meta_venta");

                                string query = "INSERT INTO Meta VALUES ('"+ empleadoID[0].InnerText+ "', '"+ mes[0].InnerText +"'," +
                                    " "+ categoria[0].InnerText +", "+ cantidad[0].InnerText +")";

                                conexion.sqlConexionAbierta();
                                conexion.sqlQuery(query);
                                conexion.sqlCerrarConexion();
                            }
                        }

                       
                    }
                }
                MessageBox.Show("Lectura de XML éxitosa", "Carga XML", MessageBoxButtons.OK, MessageBoxIcon.Information);

            /*}
            catch (Exception ex)
            {
                MessageBox.Show("Error en la lectura del XML", "Error de lectura", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }*/
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLCategorias(path);
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLProductos(path);
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLListas(path);
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLPuestos(path);
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLClientes(path);
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLEmpleados(path);
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            String path = Server.MapPath(this.FileUpload1.FileName);
            cargaXMLMetas(path);
        }
    }
}