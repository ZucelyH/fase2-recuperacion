﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularioVehiculo.aspx.cs" Inherits="FASE2.FormularioVehiculo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" href="~/Content/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="~/Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="~/Scripts/jquery-3.4.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>


            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
          <a class="navbar-brand" href="MenuAdmin.aspx">DIPROMA</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="RegistroDepartamentosMunicipios.aspx">Cargar XML <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Formularios</a>
                <div class="dropdown-menu" style="">
                  <a class="dropdown-item" href="FormularioVehiculo.aspx">Vehiculos</a>
                  <a class="dropdown-item" href="#">Clientes</a>
                  <a class="dropdown-item" href="#">Empleados</a>
                </div>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Reportes</a>
              </li>
            </ul>
          </div>
        </nav>




        <div align="center">
            <h1>Formulario Vehiculos</h1>
        </div>


        </div>
        <asp:Label ID="Label1" runat="server" Text="Ingrese placa:      "></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <br />
        <br />
        <p>
            <asp:Label ID="Label2" runat="server" Text="Ingrese motor:      "></asp:Label>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label3" runat="server" Text="Ingrese chasis:      "></asp:Label>
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label4" runat="server" Text="Ingrese marca:      "></asp:Label>
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label5" runat="server" Text="Ingrese estilo:      "></asp:Label>
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label6" runat="server" Text="Ingrese modelo:      "></asp:Label>
            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
        </p>
        <asp:Button ID="Button1" runat="server" Text="Registrar" />
    </form>
</body>
</html>
