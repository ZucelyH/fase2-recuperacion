CREATE DATABASE Fase2;
USE Fase2;
DROP DATABASE Fase2;
USE ejemplo2;




--Categoria viene en el XML
CREATE TABLE Categoria(
	idCategoria INT PRIMARY KEY NOT NULL,
	categoria VARCHAR(150) NOT NULL,
);


--Producto viene en el XML
CREATE TABLE Producto(
	idProducto INT PRIMARY KEY NOT NULL,
	nombre VARCHAR(150) NOT NULL,
	descripcion VARCHAR(250),
	categoria INT NOT NULL,
	FOREIGN KEY (categoria) REFERENCES Categoria(idCategoria),
);

--Lista viene en el XML
CREATE TABLE Lista(
	idLista INT PRIMARY KEY NOT NULL,
	nombre VARCHAR(150) NOT NULL,
    fechaInicio VARCHAR(10) NOT NULL,	
	fechaFin VARCHAR(10) NOT NULL,
);

--ListaProductos viene en el XML
CREATE TABLE ListaProductos(
	lista INT NOT NULL,
	producto INT NOT NULL,
	precio FLOAT NOT NULL,
	FOREIGN KEY (lista) REFERENCES Lista(idLista),
	FOREIGN KEY (producto) REFERENCES Producto(idProducto),
);



--Departamento viene en el XML
CREATE TABLE Departamento(
	idDepartamento INT PRIMARY KEY NOT NULL,
	departamento VARCHAR(20) NOT NULL,
);


--Ciudad  viene en el XML
CREATE TABLE Municipio(
	idMunicipio INT PRIMARY KEY NOT NULL,
	municipio VARCHAR(20) NOT NULL,
	departamento INT NOT NULL,
	FOREIGN KEY (departamento) REFERENCES Departamento(idDepartamento),
);


CREATE TABLE TipoCliente(
	idTipo INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	tipo VARCHAR(15) NOT NULL,
);


--Cliente  viene en el XML (pero no trae tipo cliente)
CREATE TABLE Cliente(
	clienteNIT VARCHAR(50) PRIMARY KEY NOT NULL,
	nombres VARCHAR(150) NOT NULL,
	apellidos VARCHAR(150),
	fechaNacimiento VARCHAR(10),
	direccion VARCHAR(200) NOT NULL,
	telefono VARCHAR(20) NOT NULL,
	celular VARCHAR(20) NOT NULL,
	email VARCHAR(25) NOT NULL,
	tipoCliente INT NOT NULL,
	municipio INT NOT NULL,
	departamento INT NOT NULL,
	limiteCredito FLOAT NOT NULL,
	diasCredito INT NOT NULL,
	lista int NOT NULL,
	FOREIGN KEY (tipoCliente) REFERENCES TipoCliente(idTipo),
	FOREIGN KEY (departamento) REFERENCES Departamento(idDepartamento),
	FOREIGN KEY (municipio) REFERENCES Municipio(idMunicipio),
	FOREIGN KEY (lista) REFERENCES Lista(idLista),
);




--Puesto viene en el XML
CREATE TABLE Puesto(
	idPuesto INT PRIMARY KEY NOT NULL,
	puesto VARCHAR(20) NOT NULL,
);


--Empleado viene en el XML
CREATE TABLE Empleado(
	NITempleado VARCHAR(150) PRIMARY KEY NOT NULL,
	nombres VARCHAR(150) NOT NULL,
	apellidos VARCHAR(150) NOT NULL, 
	fechaNacimiento VARCHAR(15),
	direccion VARCHAR(200) NOT NULL,
	telefono VARCHAR(20) NOT NULL,
	celular VARCHAR(20) NOT NULL,
	email VARCHAR(35) NOT NULL,
	puesto INT NOT NULL,
	jefe VARCHAR(150),
	passwo VARCHAR(50) NOT NULL,
	FOREIGN KEY (puesto) REFERENCES Puesto(idPuesto),
	FOREIGN KEY (jefe) REFERENCES Empleado(NITempleado),
);


--Meta  viene en el XML
CREATE TABLE Meta(
	nitEmpleado VARCHAR(150) NOT NULL,
	mes VARCHAR(10) NOT NULL,
	categoriaID INT NOT NULL,
	cantidad FLOAT NOT NULL,
	FOREIGN KEY (categoriaID) REFERENCES Categoria(idCategoria),
	FOREIGN KEY (nitEmpleado) REFERENCES Empleado(NITempleado),
);


--------------------------------------------------------------------------


CREATE TABLE Vehiculo(
	placa VARCHAR(25) IDENTITY(1,1) PRIMARY KEY NOT NULL,
	numeroMotor VARCHAR(20) NOT NULL,
	numeroChasis VARCHAR(20) NOT NULL,
	marca VARCHAR(25) NOT NULL,
	estilo VARCHAR(25) NOT NULL,
	modelo VARCHAR(25) NOT NULL,
);


CREATE TABLE MetodoPago(
	idMetodoPago INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	metodoPago VARCHAR(20) NOT NULL,
);




